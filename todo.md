This is a listing of the TODOs in the project. To fully understand the items in
this list, please make sure to read README.md on gp16/ocv-lite/.

Each one of these TODOs should be eventually opened as an issue on
gp16/ocv-lite/. If you open an issue yourself, update this file to link the
issue you opened. When the issue is resolved mark the TODO as DONE.

1. Engine
    1. AbstractCommand and Engine [DONE](https://gitlab.com/gp16/ocv-lite/issues/5)
    2. Load command [DONE](https://gitlab.com/gp16/ocv-lite/issues/1)
    3. Save command [DONE](https://gitlab.com/gp16/ocv-lite/issues/2)
    4. Free command [DONE](https://gitlab.com/gp16/ocv-lite/issues/3)
    5. Web cam capture command [DONE](https://gitlab.com/gp16/ocv-lite/issues/4)
    6. Unit tests for AbstractCommand and Engine [TODO]
    7. Unit tests for Load command [DONE](https://gitlab.com/gp16/ocv-lite/issues/6)
    8. Unit tests for Save command [DONE](https://gitlab.com/gp16/ocv-lite/issues/9)
    9. Unit tests for free command [TODO]
    10. Unit tests for Web cam capture command [DONE](https://gitlab.com/gp16/ocv-lite/issues/7)
    
2. Interpreter [TODO]
    1. Lexer (converts a stream of characters into a stream of tokens)
    2. Unit tests for command loader
    3. Command loader
        1. Load the command stated in the first command
        2. Cast arguments into objects that the command expects
        3. Execute the command with sending the arguments after casting
    4. Unit tests for command loader
    5. Batch mode (executes a whole script)
    
3. GUI [TODO]
    1. Container with dropdown to select panel type and controls to duplicate, resize and close the container
    1. Abstract panel
    2. Terminal panel
    3. Image panel
    4. Command panel
        1. Basic layout (show command name, execute button, etc,.)
        2. Command loader (method that expects a command Id and creates components of the types shown below)
        3. File chooser for SYS_PATH types
        4. Dropdown menu for selecting an image
        5. Checkbox for zero-or-one arguments
        6. Slider for small ranges (3 to 10)
        7. Dropdown to select a command
        8. Textfield for integers (accepts integers only)
        9. Textfield for floats (accepts foating point numbers only)
        10. Textfield for strings (accepts strings with the specified minimum and maximum)

4. Fix dependency problems using Ant (TODO)

5. Better logging using a library like Log4J instead of relying `System.out` (TODO)