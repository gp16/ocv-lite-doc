#### Index

- [Abstract](#abstract)
- [1. Functional requirements](#1-functional-requirements)
  - [1.1 User can use all OpenCV functionalities by using the terminal.](#11-user-can-use-all-opencv-functionalities-by-using-the-terminal)
    - [1.1.1 Introduction:](#111-introduction)
    - [1.1.2 Inputs:](#112-inputs)
    - [1.1.3 Processing:](#113-processing)
    - [1.1.4 Outputs:](#114-outputs)
  - [1.2 User can use all OpenCV functionalities by using the command interface.](#12-user-can-use-all-opencv-functionalities-by-using-the-command-interface)
    - [1.2.1 Introduction:](#121-introduction)
    - [1.2.2 Inputs:](#122-inputs)
    - [1.2.3 Processing:](#123-processing)
    - [1.2.4 Outputs:](#124-outputs)
  - [1.3 User can split any panel horizontally.](#13-user-can-split-any-panel-horizontally)
    - [1.3.1 Introduction:](#131-introduction)
    - [1.3.2 Inputs:](#132-inputs)
    - [1.3.3 Processing:](#133-processing)
    - [1.3.4 Outputs:](#134-outputs)
  - [1.4 User can split any panel vertically](#14-user-can-split-any-panel-vertically)
    - [1.4.1 Introduction:](#141-introduction)
    - [1.4.2 Inputs:](#142-inputs)
    - [1.4.3 Processing:](#143-processing)
    - [1.4.4 Outputs:](#144-outputs)
  - [1.5 User can close any panel](#15-user-can-close-any-panel)
    - [1.5.1 Introduction:](#151-introduction)
    - [1.5.2 Inputs:](#152-inputs)
    - [1.5.3 Processing:](#153-processing)
    - [1.5.4 Outputs:](#154-outputs)
  - [1.6 User can choose which UI panel to open in each screen from the four UIs (About, Image, Command, and Terminal).](#16-user-can-choose-which-ui-panel-to-open-in-each-screen-from-the-four-uis-about-image-command-and-terminal)
    - [1.6.1 Introduction:](#161-introduction)
    - [1.6.2 Inputs:](#162-inputs)
    - [1.6.3 Processing:](#163-processing)
    - [1.6.4 Outputs:](#164-outputs)
  - [1.7 In the Command UI panel, user can choose a command.](#17-in-the-command-ui-panel-user-can-choose-a-command)
    - [1.7.1 Introduction:](#171-introduction)
    - [1.7.2 Inputs:](#172-inputs)
    - [1.7.3 Processing:](#173-processing)
    - [1.7.4 Outputs:](#174-outputs)
  - [1.8 User can enter the arguments of the chosen command in the specified controls.](#18-user-can-enter-the-arguments-of-the-chosen-command-in-the-specified-controls)
    - [1.8.1 Introduction:](#181-introduction)
    - [1.8.2 Inputs:](#182-inputs)
    - [1.8.3 Processing:](#183-processing)
    - [1.8.4 Outputs:](#184-outputs)
  - [1.9 User can click the execute button to execute the chosen command.](#19-user-can-click-the-execute-button-to-execute-the-chosen-command)
    - [1.9.1 Introduction:](#191-introduction)
    - [1.9.2 Inputs:](#192-inputs)
    - [1.9.3 Processing:](#193-processing)
    - [1.9.4 Outputs:](#194-outputs)
  - [1.10 In Image UI panel, user can see all images stored in memory from a dropdown menu.](#110-in-image-ui-panel-user-can-see-all-images-stored-in-memory-from-a-dropdown-menu)
    - [1.10.1 Introduction:](#1101-introduction)
    - [1.10.2 Inputs:](#1102-inputs)
    - [1.10.3 Processing:](#1103-processing)
    - [1.10.4 Outputs:](#1104-outputs)
  - [1.11 User can choose an image to display from the dropdown menu.](#111-user-can-choose-an-image-to-display-from-the-dropdown-menu)
    - [1.11.1 Introduction:](#1111-introduction)
    - [1.11.2 Inputs:](#1112-inputs)
    - [1.11.3 Processing:](#1113-processing)
    - [1.11.4 Outputs:](#1114-outputs)
  - [1.12 User can refresh/reload the displayed image.](#112-user-can-refreshreload-the-displayed-image)
    - [1.12.1 Introduction:](#1121-introduction)
    - [1.12.2 Inputs:](#1122-inputs)
    - [1.12.3 Processing:](#1123-processing)
    - [1.12.4 Outputs:](#1124-outputs)
  - [1.13 In About UI, user can see a description of the application.](#113-in-about-ui-user-can-see-a-description-of-the-application)
    - [1.13.1 Introduction:](#1131-introduction)
    - [1.13.2 Inputs:](#1132-inputs)
    - [1.13.3 Processing:](#1133-processing)
    - [1.13.4 Outputs:](#1134-outputs)
  - [1.14 user can check on the pixel position (x,y).](#114-user-can-check-on-the-pixel-position-xy)
    - [1.14.1 Introduction:](#1141-introduction)
    - [1.14.2 Inputs:](#1142-inputs)
    - [1.14.3 processing:](#1143-processing)
    - [1.14.4 Output:](#1144-output)
- [2. Methodologies used](#2-methodolgies-used)
  - [Concepts used from Scrum](#concepts-used-from-scrum)
    - [Backlog](#backlog)
    - [Standup meetings](#standup-meetings)
  - [Concepts used from XP](#concepts-used-from-xp)
    - [Pair programming](#pair-programming)
    - [Refactoring](#refactoring)
- [3. Prototype](#3-prototype)
  - [3.1 Scope](#31-scope)
  - [3.2 Compilation and installation guide](#32-compilation-and-installation-guide)
    - [3.2.1 Downloading the source](#321-downloading-the-source)
    - [3.2.2 Building the `.jar` file](#322-building-the-jar-file)
      - [3.2.2.1 Installing Open JDK](#3221-installing-open-jdk)
      - [3.2.2.2 Installing `ant`](#3222-installing-ant)
      - [3.2.2.3 The final step](#3223-the-final-step)
    - [3.2.3 Installation](#323-installation)
  - [3.3 User manual](#33-user-manual)
    - [3.3.1 Engine](#331-engine)
      - [3.3.1.1 Commands](#3311-commands)
      - [3.3.1.2 Sample commands](#3312-sample-commands)
      - [3.3.1.3 Matrices](#3313-matrices)
      - [3.3.1.4 Example](#3314-example)
      - [3.3.1.4 Arguments and parameters](#3314-arguments-and-parameters)
    - [3.3.2 Interpreter](#332-interpreter)
      - [3.3.2.1 Interpreter modes](#3321-interpreter-modes)
      - [3.3.2.2 Syntax](#3322-syntax)
    - [3.3.3 GUI](#333-gui)
      - [3.3.3.1 Image panels](#3331-image-panels)
      - [3.3.3.2 Terminal panels](#3332-terminal-panels)
      - [3.3.3.3 Command panels](#3333-command-panels)
  - [3.4 UML diagrams of the system](#34-uml-diagrams-of-the-system)
    - [3.4.1 Class diagram of the whole project](#341-class-diagram-of-the-whole-project)
    - [3.4.2 Class diagram of AbstractCommand.java](#342-class-diagram-of-abstractcommandjava)
    - [3.4.3 Class diagram of Argument.java](#343-class-diagram-of-argumentjava)
    - [3.4.4 Class diagram of Engine.java](#344-class-diagram-of-enginejava)
    - [3.4.5 Class diagram of ICommand.java](#345-class-diagram-of-icommandjava)
    - [3.4.6 Class diagram of Parameter.java](#346-class-diagram-of-parameterjava)
    - [3.4.7 Class diagram of Type.java](#347-class-diagram-of-typejava)
    - [3.4.8 Class diagram of CmdHello.java](#348-class-diagram-of-cmdhellojava)
    - [3.4.9 Class diagram of CmdImgCapture.java](#349-class-diagram-of-cmdimgcapturejava)
    - [3.4.10 Class diagram of CmdImgDetect.java](#3410-class-diagram-of-cmdimgdetectjava)
    - [3.4.11 Class diagram of CmdImgEdge.java](#3411-class-diagram-of-cmdimgedgejava)
    - [3.4.12 Class diagram of CmdImgFlip.java](#3412-class-diagram-of-cmdimgflipjava)
    - [3.4.13 Class diagram of CmdImgFree.java](#3413-class-diagram-of-cmdimgfreejava)
    - [3.4.14 Class diagram of CmdImgGray.java](#3414-class-diagram-of-cmdimggrayjava)
    - [3.4.15 Class diagram of CmdImgLoad.java](#3415-class-diagram-of-cmdimgloadjava)
    - [3.4.16 Class diagram of CmdImgSave.java](#3416-class-diagram-of-cmdimgsavejava)
    - [3.4.17 Class diagram of Interpreter.java](#3417-class-diagram-of-interpreterjava)
    - [3.4.18 Class diagram of Main.java](#3418-class-diagram-of-mainjava)
    - [3.4.19 Class diagram of Dockable.java](#3419-class-diagram-of-dockablejava)
    - [3.4.20 Class diagram of GenericWindowContainer.java](#3420-class-diagram-of-genericwindowcontainerjava)
    - [3.4.21 Class diagram of About.java](#3421-class-diagram-of-aboutjava)
    - [3.4.22 Class diagram of Command.java](#3422-class-diagram-of-commandjava)
    - [3.4.23 Class diagram of Image.java](#3423-class-diagram-of-imagejava)
    - [3.4.24 Class diagram of Terminal.java](#3424-class-diagram-of-terminaljava)
    - [3.4.25 Class diagram of ArgumentEditor.java](#3425-class-diagram-of-argumenteditorjava)
    - [3.4.26 Class diagram of CmdIDEditor.java](#3426-class-diagram-of-cmdideditorjava)
    - [3.4.27 Class diagram of FloatEditor.java](#3427-class-diagram-of-floateditorjava)
    - [3.4.28 Class diagram of ImgIdEditor.java](#3428-class-diagram-of-imgideditorjava)
    - [3.4.29 Class diagram of IntEditor.java](#3429-class-diagram-of-inteditorjava)
    - [3.4.30 Class diagram of SmallIntEditor.java](#3430-class-diagram-of-smallinteditorjava)
    - [3.4.31 Class diagram of StringEditor.java](#3431-class-diagram-of-stringeditorjava)
    - [3.4.32 Class diagram of SystemPathEditor.java](#3432-class-diagram-of-systempatheditorjava)
    - [3.4.33 Class diagram of Engine Package](#3433-class-diagram-of-engine-package)
    - [3.4.34 Class diagram of Editors Package](#3434-class-diagram-of-editors-package)
    - [3.4.35 Flowchart for Interpreter second phase](#3435-flowchart-for-interpreter-second-phase)
  - [3.5 Design patterns used](#35-design-patterns-used)
    - [3.5.1 Singleton Pattern](#351-singleton-pattern)
    - [3.5.2 Factory Pattern](#352-factory-pattern)
      - [Step 1](#step-1)
      - [Step 2](#step-2)
    - [3.5.3 Composite Pattern](#353-composite-pattern)
  - [3.6 Javadocs](#36-javadocs)
  - [3.7 Technologies used](#37-technologies-used)
    - [3.7.1 Programming language (Java)](#371-programming-language-java)
      - [3.7.1.1 Description:](#3711-description)
      - [3.7.1.2 Uses:](#3712-uses)
    - [3.7.2 IDE (NetBeans)](#372-ide-netbeans)
      - [3.7.2.1 Description:](#3721-description)
      - [3.7.2.2 Uses:](#3722-uses)
    - [3.7.3 Computer vision library (OpenCV)](#373-computer-vision-library-opencv)
      - [3.7.3.1 Description:](#3731-description)
      - [3.7.3.2 Uses:](#3732-uses)
    - [3.7.4 GUI library (Swing)](#374-gui-library-swing)
      - [3.7.4.1 Description:](#3741-description)
      - [3.7.4.2 Uses:](#3742-uses)
    - [3.7.5 Source code documentation (Javadoc)](#375-source-code-documentation-javadoc)
      - [3.7.5.1 Description:](#3751-description)
      - [3.7.5.2 Uses:](#3752-uses)
    - [3.7.6 Version control system (Git)](#376-version-control-system-git)
      - [3.7.6.1 Description:](#3761-description)
      - [3.7.6.2 Uses:](#3762-uses)
    - [3.7.7 Automated testing (JUnit)](#377-automated-testing-junit)
      - [3.7.7.1 Description:](#3771-description)
      - [3.7.7.2 Uses:](#3772-uses)
    - [3.7.8 Build system (ant)](#378-build-system-ant)
      - [3.7.8.1 Description:](#3781-description)
      - [3.7.8.2 Uses:](#3782-uses)
    - [3.7.9 Tool for drawing diagrams (Easyuml)](#379-tool-for-drawing-diagrams-easyuml)
      - [3.7.9.1 Description:](#3791-description)
      - [3.7.9.2 Uses:](#3792-uses)
    - [3.7.10 Markdown](#3710-markdown)
      - [3.7.10.1 Description:](#37101-description)
      - [3.7.10.2 Uses:](#37102-uses)
  - [3.8 Programming HOWTOs](#38-programming-howtos)
    - [3.8.1 How to create a new command](#381-how-to-create-a-new-command)
    - [3.8.2 How to create a new UI dockable](#382-how-to-create-a-new-ui-dockable)
    - [3.8.3 How to create a new test file](#383-how-to-create-a-new-test-file)
  - [3.9 Testing](#39-testing)
    - [3.9.1 Test case ID: 1](#391-test-case-id-1)
      - [3.9.1.1 Purpose:](#3911-purpose)
      - [3.9.1.2 System requirements covered:](#3912-system-requirements-covered)
      - [3.9.1.3 Preconditions:](#3913-preconditions)
      - [3.9.1.4 Inputs:](#3914-inputs)
      - [3.9.1.5 post conditions:](#3915-post-conditions)
      - [3.9.1.6 user actions(steps):](#3916-user-actionssteps)
      - [3.9.1.7 Expected result:](#3917-expected-result)
      - [3.9.1.8 Actual result:](#3918-actual-result)
      - [3.9.1.9 pass/fail/untested:](#3919-passfailuntested)
    - [3.9.2 Test case ID: 2](#392-test-case-id-2)
      - [3.9.2.1 Purpose:](#3921-purpose)
      - [3.9.2.2 System requirements covered:](#3922-system-requirements-covered)
      - [3.9.2.3 Preconditions:](#3923-preconditions)
      - [3.9.2.4 Inputs:](#3924-inputs)
      - [3.9.2.5 post conditions:](#3925-post-conditions)
      - [3.9.2.6 user actions(steps):](#3926-user-actionssteps)
      - [3.9.2.7 Expected result:](#3927-expected-result)
      - [3.9.2.8 Actual result:](#3928-actual-result)
      - [3.9.2.9 pass/fail/untested:](#3929-passfailuntested)
    - [3.9.3 Test case ID: 3](#393-test-case-id-3)
      - [3.9.3.1 Purpose:](#3931-purpose)
      - [3.9.3.2 System requirements covered:](#3932-system-requirements-covered)
      - [3.9.3.3 Preconditions:](#3933-preconditions)
      - [3.9.3.4 Inputs:](#3934-inputs)
      - [3.9.3.5 post conditions:](#3935-post-conditions)
      - [3.9.3.6 user actions(steps):](#3936-user-actionssteps)
      - [3.9.3.7 Expected result:](#3937-expected-result)
      - [3.9.3.7 Actual result:](#3937-actual-result)
      - [3.9.3.8 pass/fail/untested:](#3938-passfailuntested)
    - [3.9.4 Test case ID: 4](#394-test-case-id-4)
      - [3.9.4.1 Purpose:](#3941-purpose)
      - [3.9.4.2 System requirements covered:](#3942-system-requirements-covered)
      - [3.9.4.3 Preconditions:](#3943-preconditions)
      - [3.9.4.4 Inputs:](#3944-inputs)
      - [3.9.4.5 post conditions:](#3945-post-conditions)
      - [3.9.4.6 user actions(steps):](#3946-user-actionssteps)
      - [3.9.4.7 Expected result:](#3947-expected-result)
      - [3.9.4.8 Actual result:](#3948-actual-result)
      - [3.9.4.9 pass/fail/untested:](#3949-passfailuntested)
    - [3.9.5 Test case ID: 5](#395-test-case-id-5)
      - [3.9.5.1 Purpose:](#3951-purpose)
      - [3.9.5.2 System requirements covered:](#3952-system-requirements-covered)
      - [3.9.5.3 Preconditions:](#3953-preconditions)
      - [3.9.5.4 Inputs:](#3954-inputs)
      - [3.9.5.5 post conditions:](#3955-post-conditions)
      - [3.9.5.6 user actions(steps):](#3956-user-actionssteps)
      - [3.9.5.7 Expected result:](#3957-expected-result)
      - [3.9.5.8 Actual result:](#3958-actual-result)
      - [3.9.5.9 pass/fail/untested:](#3959-passfailuntested)
    - [3.9.6 Test case ID: 6](#396-test-case-id-6)
      - [3.9.6.1 Purpose:](#3961-purpose)
      - [3.9.6.2 System requirements covered:](#3962-system-requirements-covered)
      - [3.9.6.3 Preconditions:](#3963-preconditions)
      - [3.9.6.4 Inputs:](#3964-inputs)
      - [3.9.6.5 post conditions:](#3965-post-conditions)
      - [3.9.6.6 user actions(steps):](#3966-user-actionssteps)
      - [3.9.6.7 Expected result:](#3967-expected-result)
      - [3.9.6.8 Actual result:](#3968-actual-result)
      - [3.9.6.9 pass/fail/untested:](#3969-passfailuntested)
    - [3.9.7 Test case ID: 7](#397-test-case-id-7)
      - [3.9.7.1 Purpose:](#3971-purpose)
      - [3.9.7.2 System requirements covered:](#3972-system-requirements-covered)
      - [3.9.7.3 Preconditions:](#3973-preconditions)
      - [3.9.7.4 Inputs:](#3974-inputs)
      - [3.9.7.5 post conditions:](#3975-post-conditions)
      - [3.9.7.6 user actions(steps):](#3976-user-actionssteps)
      - [3.9.7.7 Expected result:](#3977-expected-result)
      - [3.9.7.8 Actual result:](#3978-actual-result)
      - [3.9.7.9 pass/fail/untested:](#3979-passfailuntested)
    - [3.9.8 Test case ID: 8](#398-test-case-id-8)
      - [3.9.8.1 Purpose:](#3981-purpose)
      - [3.9.8.2 System requirements covered:](#3982-system-requirements-covered)
      - [3.9.8.3 Preconditions:](#3983-preconditions)
      - [3.9.8.4 Inputs:](#3984-inputs)
      - [3.9.8.5 post conditions:](#3985-post-conditions)
      - [3.9.8.6 user actions(steps):](#3986-user-actionssteps)
      - [3.9.8.7 Expected result:](#3987-expected-result)
      - [3.9.8.8 Actual result:](#3988-actual-result)
      - [3.9.8.9 pass/fail/untested:](#3989-passfailuntested)
    - [3.9.9 Test case ID: 9](#399-test-case-id-9)
      - [3.9.9.1 Purpose:](#3991-purpose)
      - [3.9.9.2 System requirements covered:](#3992-system-requirements-covered)
      - [3.9.9.3 Preconditions:](#3993-preconditions)
      - [3.9.9.4 Inputs:](#3994-inputs)
      - [3.9.9.5 post conditions:](#3995-post-conditions)
      - [3.9.9.6 user actions(steps):](#3996-user-actionssteps)
      - [3.9.9.7 Expected result:](#3997-expected-result)
      - [3.9.9.8 Actual result:](#3998-actual-result)
      - [3.9.9.9 pass/fail/untested:](#3999-passfailuntested)
    - [3.9.10 Test case ID: 10](#3910-test-case-id-10)
      - [3.9.10.1 Purpose:](#39101-purpose)
      - [3.9.10.2 System requirements covered:](#39102-system-requirements-covered)
      - [3.9.10.3 Preconditions:](#39103-preconditions)
      - [3.9.10.4 Inputs:](#39104-inputs)
      - [3.9.10.5 post conditions:](#39105-post-conditions)
      - [3.9.10.6 user actions(steps):](#39106-user-actionssteps)
      - [3.9.10.7 Expected result:](#39107-expected-result)
      - [3.9.10.8 Actual result:](#39108-actual-result)
      - [3.9.10.9 pass/fail/untested:](#39109-passfailuntested)
    - [3.9.11 Test case ID: 11](#3911-test-case-id-11)
      - [3.9.11.1 Purpose:](#39111-purpose)
      - [3.9.11.2 System requirements covered:](#39112-system-requirements-covered)
      - [3.9.11.3 Preconditions:](#39113-preconditions)
      - [3.9.11.4 Inputs:](#39114-inputs)
      - [3.9.11.5 post conditions:](#39115-post-conditions)
      - [3.9.11.6 user actions(steps):](#39116-user-actionssteps)
      - [3.9.11.7 Expected result:](#39117-expected-result)
      - [3.9.11.8 Actual result:](#39118-actual-result)
      - [3.9.11.9 pass/fail/untested:](#39119-passfailuntested)
    - [3.9.12 Test case ID: 12](#3912-test-case-id-12)
      - [3.9.12.1 Purpose:](#39121-purpose)
      - [3.9.12.2 System requirements covered:](#39122-system-requirements-covered)
      - [3.9.12.3 Preconditions:](#39123-preconditions)
      - [3.9.12.4 Inputs:](#39124-inputs)
      - [3.9.12.5 post conditions:](#39125-post-conditions)
      - [3.9.12.6 user actions(steps):](#39126-user-actionssteps)
      - [3.9.12.7 Expected result:](#39127-expected-result)
      - [3.9.12.8 Actual result:](#39128-actual-result)
      - [3.9.12.9 pass/fail/untested:](#39129-passfailuntested)
- [3.10 Faced Problems](#310-faced-problems)
  - [3.10.1 Technical Problems.](#3101-technical-problems)
    - [3.10.1.1 Load OpenCV library on different operating systems.](#31011-load-opencv-library-on-different-operating-systems)
    - [3.10.1.2 Different versions of OpenCV and merging codes generated by each one of them.](#31012-different-versions-of-opencv-and-merging-codes-generated-by-each-one-of-them)
    - [3.10.1.3 Determining which service provider to use for "Git" technology](#31013-determining-which-service-provider-to-use-for-git-technology)
  - [3.10.2 Non-Technical Problems](#3102-non-technical-problems)
    - [3.10.2.1 Not able to use some service providers For money and members number issues](#31021-not-able-to-use-some-service-providers-for-money-and-members-number-issues)
- [4. Current semester changes](#4-current-semester-changes)
  - [4.1 Scope](#41-scope)
  - [4.2 Architectural changes](#42-architectural-changes)
    - [4.2.1 Plugin support](#421-plugin-support)
  - [4.3 New technologies](#43-new-technologies)
  - [5. Roles](#5-roles)
    - [5.1 Abdelrahman Mohsen](#51-abdelrahman-mohsen)
    - [5.2 Al Mohannad Haroon](#52-al-mohannad-haroon)
    - [5.3 Amr Ayman](#53-amr-ayman)
    - [5.4 Amr Gamal](#54-amr-gamal)
    - [5.5 Mohammad Helmy](#55-mohammad-helmy)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Abstract
Many programming libraries have notably practical applications in every day life.
However, more often than not, such libraries are intended for users with solid
development background. This forms a barrier for the average layman that may
prevent him from unlocking the full potential of the already existing libraries.
Our project intends to develop a systematic approach for generating user-friendly
graphical interfaces for these libraries. Not only will the average layman
benefit from this, but also developers who want to explore new libraries, test
their ideas in an interactive manner, and many more. In this paper, we will detail
our progress in implementing these concepts on the widely used image processing
library, OpenCV.


# 1. Functional requirements
## 1.1 User can use all OpenCV functionalities by using the terminal. 
### 1.1.1 Introduction: 
For users who will be using the terminal instead of the Command UI, they need to write both the command name and command arguments in a text field. Also they need a way to execute these commands. They can do that by pressing the ENTER key on the keyboard.  
### 1.1.2 Inputs: 
Command name/s and its arguments as statements. 
### 1.1.3 Processing: 
Terminal sends the statements written in the text field to the interpreter, the interpreter 
then divides the statements into command name and arguments, 
and execute these arguments on the command.
### 1.1.4 Outputs: 
First: the command name will appear in the terminal.
Then:
In case of images, Image/modified image is added to the dropdown menu of the images in image UI panel. 
In case of text, the text will appear in the terminal.
## 1.2 User can use all OpenCV functionalities by using the command interface. 
### 1.2.1 Introduction: 
User can choose a command name from the command dropdown menu, and write the arguments for that command in the specified UI components.
### 1.2.2 Inputs: 
Command name and command arguments.
### 1.2.3 Processing: 
When the user clicks on the execute button, the application execute the command based on the arguments. 
### 1.2.4 Outputs: 
Image/modified image is added to the dropdown menu of the images in image UI panel.
## 1.3 User can split any panel horizontally. 
### 1.3.1 Introduction: 
All panels can be spited horizontally in to two panels of the same type as the opened one.
### 1.3.2 Inputs: 
Mouse click on the percentage button
### 1.3.3 Processing: 
The panel application creates a new panel of the same type as the panel containing the percentage button that was clicked on.
### 1.3.4 Outputs: 
A new panel of the same type is created next to the opened panel horizontally.
## 1.4 User can split any panel vertically
### 1.4.1 Introduction: 
All panels can be spited vertically in to two panels of the same type as the opened one.
### 1.4.2 Inputs: 
Mouse click on the divide button
### 1.4.3 Processing: 
The panel application creates a new panel of the same type as the panel containing the percentage button that was clicked on.
### 1.4.4 Outputs: 
A new panel of the same type is created under the opened panel vertically.
## 1.5 User can close any panel
### 1.5.1 Introduction: 
Every panel has a button labeled ‘X’ which a user can click on to close that panel.
### 1.5.2 Inputs: 
Mouse click on the ‘X’ button.
### 1.5.3 Processing: 
Application discards that panel and reuse its space for the rest of the opened panels. 
### 1.5.4 Outputs: 
A panel gets discarded/closed.
## 1.6 User can choose which UI panel to open in each screen from the four UIs (About, Image, Command, and Terminal).
### 1.6.1 Introduction: 
The application provides the user with four UI panels listed in a dropdown menu in every panel, where the user selects one of them for each opened panel in the application. 
### 1.6.2 Inputs:
UI panel name.
### 1.6.3 Processing:
The application opens the chosen UI in the opened panel.
### 1.6.4 Outputs:
Selected UI panel is opened. 
## 1.7 In the Command UI panel, user can choose a command.
### 1.7.1 Introduction: 
User can select a command name from a dropdown menu that contains all commands registered in the engine.
### 1.7.2 Inputs: 
Command name from the dropdown menu
### 1.7.3 Processing: 
The application creates proper UI components for the chosen command, for example: text fields for the string arguments, chooser for paths…etc. 
### 1.7.4 Outputs: 
Proper UI components for the chosen commands. 
## 1.8 User can enter the arguments of the chosen command in the specified controls.
### 1.8.1 Introduction: 
In the UI components, user is able to enter the arguments for the chosen command to be able to execute the command.
### 1.8.2 Inputs: 
Arguments of the chosen command.
### 1.8.3 Processing: 
User types or select values for the arguments of the chosen command in its proper area.
### 1.8.4 Outputs: 
None. 
## 1.9 User can click the execute button to execute the chosen command.
### 1.9.1 Introduction: 
After the user adds the arguments for the command, the user needs a way to start the execution of these arguments on the command, the user can do that by clicking on the execute button.
### 1.9.2 Inputs: 
Mouse click on the execute button.
### 1.9.3 Processing: 
The application executes the arguments on the chosen command.
### 1.9.4 Outputs: 
In case of images, Image/modified image is added to the dropdown menu of the images in image UI panel. 
In case of text, there will be no output.
## 1.10 In Image UI panel, user can see all images stored in memory from a dropdown menu.
### 1.10.1 Introduction: 
All the images stored in memory appear in the dropdown menu of the images in the UI panel only.
### 1.10.2 Inputs: 
Mouse click on the dropdown menu. 
### 1.10.3 Processing: 
Application checks the memory for the stored image names and display them in the dropdown menu with a default choose option.
### 1.10.4 Outputs: 
Image names of all images in the dropdown menu.
## 1.11 User can choose an image to display from the dropdown menu.
### 1.11.1 Introduction: 
When a user chooses an image name from the dropdown menu, the application displays that image to the user.
### 1.11.2 Inputs: 
Name of the image stored in memory
### 1.11.3 Processing: 
The application gets that image MAT and converts it to an image then display that image to the user.
### 1.11.4 Outputs: 
An image displayed in the Image UI panel.
## 1.12 User can refresh/reload the displayed image.
### 1.12.1 Introduction: 
User needs a way to refresh the image once some modification to the image are made. To do that, the user can click on the refresh button.
### 1.12.2 Inputs: 
Name of the image stored in memory. 
### 1.12.3 Processing: 
Application gets the name of the displayed image from the dropdown menu and get its matrix from the memory, converts it to an image, then redisplay it in the Image UI panel.
### 1.12.4 Outputs: 
An image redisplayed in the Image UI panel. 
## 1.13 In About UI, user can see a description of the application.
### 1.13.1 Introduction: 
When users launch the application for the first time, they may want to know about this application, they can do that using the About UI.
### 1.13.2 Inputs:  
UI name.
### 1.13.3 Processing: 
The application opens the chosen UI in the opened panel. 
### 1.13.4 Outputs: 
About UI appears. 
## 1.14 user can check on the pixel position (x,y).
### 1.14.1 Introduction:
user can check the position of any pixel for later usage simply by clicking on the pixel on the image.
### 1.14.2 Inputs:
mouse click on any pixel in the image.
### 1.14.3 processing:
When the user clicks on a pixel, the application gets the position of this pixel by getting the position of this pixel from x and y axis.
### 1.14.4 Output:
The X and Y values are written above the image.
## 1.15 User can resize any opened image.
### 1.15.1 Introduction:
The user can resize any image opened in an image-UI panel dynamically at runtime
### 1.15.2 Inputs:
Mouse click and hold one of the panel's borders and drag to any direction
### 1.15.3 processing:
When the user clicks and drags a panel's border the image/s opened inside one or more panels in the same window gets resized as the user is dragging the border.
### 1.15.4 Output:
An image/s that is resized dynamically at runtime.
## 1.16 User can search for a specific command in the command UI.
### 1.15.1 Introduction:
Some users may not want to traverse through the list of available commands, they can then search for their desired command from the combo box containing all the command names.
### 1.16.2 Inputs:
A command name
### 1.16.3 processing:
The system searches through the command list as the user is typing character by character and provide the user with the available commands that matches the same sequence of characters that the user is entering.
### 1.16.4 Output:
The command that is desired by the user.
# 2. Methodologies used

We used a hybrid between XP and Scrum.

## Concepts used from Scrum

### Backlog

Backlog was one of the concepts we used from Scrum and it was implemented
as milestones on gitlab.com.

1) Sprint 1: Engine
- Interface for all commands
- Engine to keep track of images and commands
- Load command
- Save command
- Free command

2) Sprint 2: Interpreter
- Split statements into command name and arguments
- Load command
- Create argument objects from the string
- Send arguments to the command

3) Sprint 3: GUI
- Docking framework
- Terminal
- Command
- Argument editors

### Standup meetings

We often had daily standup meetings that lasted for about for about 15 minutes
in college. However we eventually switched to virtual meeting software like
Google Hangouts.


## Concepts used from XP
### Pair programming 
We usually have pair programming sessions in order to implement to resolve bugs,
enhance existing features and implement new features.

### Refactoring
We used to do some refactoring after finishing each sprint. This including 
renaming some variables to better names and fixing code formatting.
# 3. Prototype
## 3.1 Scope
We have implemented a fully functional prototype based on the previously mentioned
concepts. It interfaces with the existing image processing library OpenCV.

## 3.2 Compilation and installation guide

This installation procedure assumes a UNIX environment, the exact installation and
compilation procedure were tested on Ubuntu 12.04 LTS, however it should work
on different apt-based distros unchanged. Other systems may require slight
modifications but may undergo the same logic.
### 3.2.1 Downloading the source
Source code is hosted on gitlab.com, a version control hosting service. It's
assumed that you have a working git installation since it is the tool used to
fetch the sources.
Open a terminal and type

    git clone git@gitlab.com:gp16/ocv-lite.git
    
You might be prompted to enter login credentials since the project is hosted
privately for now. If you want to get access to the repository, please contact
us and we will set it up for you.

If the last command was executed successfully, you should by now have a directory
called `ocv-lite` containing the source code and history of the prototype.

### 3.2.2 Building the `.jar` file
Before building the project you first need to have the compile time dependencies,
which are JDK (version 1.8 or higher) and `ant`.

#### 3.2.2.1 Installing Open JDK
The project has been tested both on openjdk and Oracle JDK.
To install Oracle JDK type
    
    # add Java backports to your sources.list
    sudo add-apt-repository ppa:webupd8team/java
    sudo apt-get update
    
    # accept the license
    sudo echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
    sudo echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
    
    # install jdk 8 and set it as the default version
    sudo apt-get install oracle-java8-installer oracle-java8-set-default

Make sure the installation proceeded successfully by issuing the command
`java -version`

#### 3.2.2.2 Installing `ant`
`ant` is the build system that the project uses in order to instruct the Java
compiler, `javac`, how to collect dependencies, how to link them together, and
finally, how to produce a runnable `.jar` file.

Installing `ant` is as simple as

    sudo apt-get install ant

#### 3.2.2.3 The final step
Now you have all the compilation dependencies and ready to actually compile
the project.

    # enter the source directory
    cd ./ocv-lite/
    
    # compile everything
    ant compile

After doing the previous step, the output can be found at `./dist/ocvlite.jar`.
Refer to the next section for instructions on how to install this file.

### 3.2.3 Installation
As mentioned earlier, the goal of the project is to interface with the already
existing library, OpenCV, which means we have to install it. The prototype currently
supports version `2.4.9`.

Here is how to install OpenCV `2.4.9`
    

    # get out of ./ocv-lite if you are still there
    cd ..

    # needed to compile JNI files
    export JAVA_HOME=/usr/lib/jvm/java-8-oracle
    
    # install the build system for OpenCV
    sudo apt-get install cmake -y
    
    # download OpenCV sources
    git clone https://github.com/Itseez/opencv.git
    
    # enter the downloaded repo
    cd opencv/
    
    # switch to the correct version
    git checkout 2.4.9
    
    # start the build process
    mkdir build
    cd build
    cmake ..
    make -j8
    
    cd ../../

Now that you have OpenCV installed, you can run the prototype by typing

    java -jar ocv-lite/dist/ocvlite.jar -Djava.libray.path=opencv/build/lib

## 3.3 User manual

ocv-lite is a project that aims to simplify using OpenCV by providing
easy-to-use wrappers around it. There are essentially three wrappers,
namely, the engine, the GUI frontend, and the interpreter. Each of
these parts serve a different purpose as we will see in the subsequent
sections.

### 3.3.1 Engine
The engine is the core component of the project that handles all the OpenCV logic.
Its main purpose is to power the other two wrappers, the interpreter and the
GUI. The engine is meant to be an internal part hidden from the end-user. And
indeed, the user doesn't need to it directly since it is guaranteed that the
other wrappers have all the capabilities the engine has. With that said, there
are no technical barriers preventing the user from accessing the engine directly
if he wishes.

#### 3.3.1.1 Commands
The engine is responsible for encapsulating OpenCV functionalities and allowing
the developer to access them through a simple API. These functionalities are
encapsulated into objects of type `ICommand`, which is the building block of
the engine. You can think of an `ICommand` as a callback that executes a
specific task related to OpenCV. For example, loading an image, converting it to
greyscale, performing edge detection on it, etc,. Each of these commands is
identified by a name. For example, the command responsible for loading an image
is identified by the name "load". In order to access a command of the engine and
execute it, you need to know its name.

#### 3.3.1.2 Sample commands
Here is a list of some commands to give you an idea of what commands are about

1. CmdImgLoad
2. CmdImgSave
3. CmdImgFree
4. CmdImgCapture
5. CmdImgFlip

#### 3.3.1.3 Matrices
Matrices are used internally to represent images. The engine keeps track of the
matrices (internally represented as matrices) it is working on. Each matrix is
identified by a string, which we sometimes refer to as the MAT_Id. To store a
matrix in the engine memory, you need provide a unique Id for it. And when you
need to access the same matrix again, you will need to just provide the same Id
again.

#### 3.3.1.4 Example
Here is an example of how to use the API to load an image from disk, flip it,
and save the resulting image on disk again.

```
// --- Get reference to the engine --- //
Engine engine = Engine.getInstance();

// --- 1) Load the image --- //
// Get reference to the load command
ICommand load = engine.getCommand("load");

// The load command expects two arguments, the first is the path to the
// image on disk, the second is the new image Id
load.execute("/path/to/image.png", "myImage1");


// --- 2) Flip it --- //
// Get reference to the flip command
ICommand flip = engine.getCommand("flip");

// The flip command expects two image Ids, source and destination
flip.execute("myImage1", "myImage2");


// --- 3) Save the result --- //
// Get reference to the save command
ICommand save = engine.getCommand("save");

// The save command expects two arguments, the image id and path on disk
save.execute("myImage2", "/path/to/flipped.png");
```

The reason behind using string names to access commands is that this is the only
way the interpreter can refer to a command. Because the input to the
interpreter is a script which is, of course, in plaintext.

#### 3.3.1.4 Arguments and parameters

Commands are not very permissive when it comes to receiving arguments. If it
expects the first argument to be a string, then it must be. Sending an argument
of wrong type will raise an error. And so will sending too many or too few
arguments.

This raises a question, how does a command decide whether the arguments are
valid or not? The answer to this question has more to do with how the API is
programmed than how it is be used. There are two mechanisms to ensure that the
passed arguments are valid.

The first mechanism allows each `ICommand` to specify an array of `Parameter`
objects. This array serves as a function signature in procedural languages like
C, it is used as a reference to tell whether an array of arguments is valid or
not. To make the separation between parameters and arguments clearer, think of a
parameter array as a property of a command and an argument array what is
actually passed to the command.

The second mechanism is type checking. The array of arguments is checked against
the array of parameters to see if they are valid or not. In fact, it is not
guaranteed that all `ICommand` objects implement type checking. This is why the
API provides the `AbstractCommand` class, which implements `ICommand` and
provides some basic traits like type checking, argument loading, usage manual,
etc,. On a side note, command programmers are encouraged to extend
`AbstractCommand` rather than implement `ICommand` directly.

In order to execute a command, you should send it a valid array of arguments.
Fortunately, you can use `AbstractCommand.showFullMan()`, which displays
everything about the parameters, including their names, their types, what they
are used for, and whether they are optional.

### 3.3.2 Interpreter
The interpreter exposes the engine commands to the front-end user in a
systematic way. The user is allowed to write statements and send them to the 
interpreter, which executes them one by one in a sequential order. A statement
is a string of text that instructs the interpreter to do something. It should
contain the name of the engine command to be executed and the arguments to be
sent to it.

#### 3.3.2.1 Interpreter modes
The interpreter can be used either in interactive or batch mode. In the
interactive mode, the interpreter waits for the user to manually enter the
statements and executes each statements once it is entered. In the batch mode,
the user can group all the commands he wants to execute in one file and then
feed it to the interpreter.

To run the interpreter in interactive mode, type into terminal
```
java -jar ocvlite.jar -i
```
And for batch mode
```
java -jar ocvlite.jar -i /path/to/script.ocvl
```

#### 3.3.2.2 Syntax
A script is a list of statements separated by new lines. Each statement starts
with the command name followed by it arguments. For example,
```
    load p"/path/to/image.png" img1
    flip img1 img2 
    resize img2 img3 2.0 
    save img3 p"/path/to/result.png"
```
The above script loads the image named "image.png", flips it, doubles its size,
and then saves it as "result.png".

Different argument types are written differently
1. Strings are enclosed within single quotes
2. System paths are enclosed within single quotes with a *p* prefix
3. Command Ids are enclosed within single quotes with a *c* prefix
4- Matrix Ids, integers and floats are not enclosed within anything

The arguments that the interpreter can recognize are integers, floats, and
strings. The interpreter is then responsible for casting these arguments
including strings into types that the engine expects like STR, SYS_PATH, MAT_ID,
CMD_ID, etc,. The correct type is decided based on the parameters of the stated
command in the statement.

Each statement follows this rough synopsis
```
<statement>: <cmd_name> [ "<string>" | <integer> | <float> ] ...
```


### 3.3.3 GUI
The GUI defines a new type of panels. Each one of them has the required controls
to allow the user to resize, close, or duplicate it. Any panel can be duplicated
either vertically or horizontally.
```
.----------.                                   .-----------.
|          |                                   |     |     |
|          |   -- duplicated vertically -->    |     |     |
|          |                                   |     |     |
`----------`                                   `-----------`

.----------.                                   .-----------.
|          |                                   |           |
|          |   -- duplicated horizontally -->  |-----------|
|          |                                   |           |
`----------`                                   `-----------`
```
The reason you might want to duplicate panels is to add new panels of different
types to the screen. Although the new panels initially display the content
displayed by the older ones, every panel has a dropdown menu to select what
content it should display. So after you duplicate a panel, click the dropdown
menu and select the content you want. This content can be either an image, a
command, or a terminal.

#### 3.3.3.1 Image panels
Image panels are assigned a matrix Id to display its content as an image. The
panel is updated once the image is updated by the engine. The user can select the
image id from a dropdown menu or enter it manually if it doesn't exist.

#### 3.3.3.2 Terminal panels
A terminal is simply a graphical interface to the interactive mode of the
interpreter. This is the only panel shown on the screen when the user starts the
GUI. It displays 2 GUI components to the user, a text field where the user writes
the  statements to be executed and a text area that shows the result of each command.

#### 3.3.3.3 Command panels
A command panel is a graphical representation of a command recognized by the
engine. The user selects the requested command from a dropdown menu. 
The command panel then inspects the requested command and shows the necessary inputs 
and controls to set its arguments. Here is how the different arguments are displayed

1. STR: textfield
1. SYS_PATH: file chooser 
2. MAT_ID: dropdown menu (or manually enter matrix Id)
4. INT with min=0, max=1: checkbox
5. INT with small ranges (3, 10): slider
6. INT with larger ranges: textfield (accepts integers only)
7. FLOAT: textfield (accepts floats only)
8. CMD_ID: dropdown menu

When the user finishes entering the correct arguments, he can click then
'execute' which is available on every command panel.
## 3.4 UML diagrams of the system

### 3.4.1 Class diagram of the whole project 
![Class Diagram](classDiagrams/classdiagram.png)
### 3.4.2 Class diagram of AbstractCommand.java 
![Class Diagram](classDiagrams/AbstractCommand.png)
### 3.4.3 Class diagram of Argument.java 
![Class Diagram](classDiagrams/Argument.png)
### 3.4.4 Class diagram of Engine.java
![Class Diagram](classDiagrams/Engine.png)
### 3.4.5 Class diagram of ICommand.java
![Class Diagram](classDiagrams/ICommand.png)
### 3.4.6 Class diagram of Parameter.java
![Class Diagram](classDiagrams/Parameter.png)
### 3.4.7 Class diagram of Type.java 
![Class Diagram](classDiagrams/Type.png)
### 3.4.8 Class diagram of CmdHello.java
![Class Diagram](classDiagrams/CmdHello.png)
### 3.4.9 Class diagram of CmdImgCapture.java 
![Class Diagram](classDiagrams/capture.png)
### 3.4.10 Class diagram of CmdImgDetect.java
![Class Diagram](classDiagrams/detect.png)
### 3.4.11 Class diagram of CmdImgEdge.java 
![Class Diagram](classDiagrams/edge.png)
### 3.4.12 Class diagram of CmdImgFlip.java 
![Class Diagram](classDiagrams/flip.png)
### 3.4.13 Class diagram of CmdImgFree.java 
![Class Diagram](classDiagrams/free.png)
### 3.4.14 Class diagram of CmdImgGray.java
![Class Diagram](classDiagrams/gray.png)
### 3.4.15 Class diagram of CmdImgLoad.java
![Class Diagram](classDiagrams/load.png)
### 3.4.16 Class diagram of CmdImgSave.java 
![Class Diagram](classDiagrams/save.png)
### 3.4.17 Class diagram of Interpreter.java 
![Class Diagram](classDiagrams/interpreter.png)
### 3.4.18 Class diagram of Main.java 
![Class Diagram](classDiagrams/main.png)
### 3.4.19 Class diagram of Dockable.java 
![Class Diagram](classDiagrams/dockable.png)
### 3.4.20 Class diagram of GenericWindowContainer.java 
![Class Diagram](classDiagrams/GenericWindowContainer.png)
### 3.4.21 Class diagram of About.java 
![Class Diagram](classDiagrams/about.png)
### 3.4.22 Class diagram of Command.java 
![Class Diagram](classDiagrams/command.png)
### 3.4.23 Class diagram of Image.java 
![Class Diagram](classDiagrams/image.png)
### 3.4.24 Class diagram of Terminal.java 
![Class Diagram](classDiagrams/terminal.png)
### 3.4.25 Class diagram of ArgumentEditor.java 
![Class Diagram](classDiagrams/ArgumentEditor.png)
### 3.4.26 Class diagram of CmdIDEditor.java 
![Class Diagram](classDiagrams/CmdIdEditor.png)
### 3.4.27 Class diagram of FloatEditor.java 
![Class Diagram](classDiagrams/floateditor.png)
### 3.4.28 Class diagram of ImgIdEditor.java
![Class Diagram](classDiagrams/imgideditor.png)
### 3.4.29 Class diagram of IntEditor.java 
![Class Diagram](classDiagrams/inteditor.png)
### 3.4.30 Class diagram of SmallIntEditor.java 
![Class Diagram](classDiagrams/smallinteditor.png)
### 3.4.31 Class diagram of StringEditor.java 
![Class Diagram](classDiagrams/stringeditor.png)
### 3.4.32 Class diagram of SystemPathEditor.java 
![Class Diagram](classDiagrams/systempatheditor.png)
### 3.4.33 Class diagram of Engine Package 
![Class Diagram](classDiagrams/enginepackage.png)
### 3.4.34 Class diagram of Editors Package 
![Class Diagram](classDiagrams/editorspackage.png)
### 3.4.35 Flowchart for Interpreter second phase 
![Class Diagram](classDiagrams/flowchart.jpg)

## 3.5 Design patterns used
### 3.5.1 Singleton Pattern 
Singleton pattern involves a single class which is responsible to create an object while making sure that only single object gets created. in our case, The Engine constructor is private so no other classes can create new instance of the engine, instead using `getInstance()` method.

![Class Diagram](classDiagrams/engine.png)

```
private Engine() {
    registerCommand(new CmdHello());    
    registerCommand(new CmdImgCapture());
        registerCommand(new CmdImgLoad());
        registerCommand(new CmdImgFree());
        registerCommand(new CmdImgSave());
        registerCommand(new CmdImgGray());
        registerCommand(new CmdImgFlip());
        registerCommand(new CmdImgEdge());
        registerCommand(new CmdImgDetect());
        registerCommand(new CmdMan());
    }

``` 

```
public static Engine getInstance() {
    if(instance == null)
        instance = new Engine();
    
    return instance;
    }

```
### 3.5.2 Factory Pattern 
Factory pattern is a creational pattern as it is used to create objects, in our case, we use factory pattern to create an editor for each data type without exposing the creation logic, but referring to it using a common interface (Argument Editor).

![Class Diagram](designPatterns/argumenteditors.jpg)

#### Step 1 
Creating an Interface 

```
public interface ArgumentEditor {

    public Component getEditorPanel();

    public Argument getArgument();

    public void setParameter(Parameter param);

    public Parameter getParameter();

    public boolean isArgumentValid();

    public static ArgumentEditor createInstance(Parameter param) {
    }

}
```
#### Step 2 
Create concrete classes implementing the same interface.

```
    public class ImgIdEditor extends JPanel implements ArgumentEditor {
    public ImgIdEditor() {
    imageID = new JComboBox(Engine.getInstance().getImagesNames());
    label = new JLabel();
    imageID.setEditable(true);
    add(label);
    add(imageID);
    }

    @Override
    public Component getEditorPanel() {
        return this;
    }

```
### 3.5.3 Composite Pattern 
Composite pattern is used where we need to treat a group of objects in similar way as a single object. in our case, user can duplicate Directed Window Container horizontally or vertically or any other component on itself.

![Class Diagram](designPatterns/composite.jpg)


```
public class GenericWindowContainer extends JPanel {
        
    public static void registerDockableClass(Class clazz) {
    DockingWindow.registerDockableClass(clazz);
    }
    
    static {
    registerDockableClass(About.class);
    registerDockableClass(Command.class);   
        registerDockableClass(Image.class);
        registerDockableClass(Terminal.class);
    }
    .
    .
    .
    .
 }

```

```
class DirectedWindowContainer extends JXMultiSplitPane {
    
    private final MultiSplitLayout.Split model;
    private final ArrayList<MultiSplitLayout.Node> nodes;
    private final MultiSplitLayout layout;
    
    public DirectedWindowContainer(boolean horizontal) {
    model = new Split();
    model.setRowLayout(horizontal);
    layout = getMultiSplitLayout();
    nodes = (ArrayList<Node>) model.getChildren();
    }
    .
    .
    .
    .
 }

```

## 3.6 Javadocs
[Java documentation](../javadoc/index.html)
## 3.7 Technologies used
### 3.7.1 Programming language (Java)
#### 3.7.1.1 Description:
Java is a high level programming language and computing platform and enforces an object-oriented programming model. 
#### 3.7.1.2 Uses:
We used Java to create whole application by creating classes, interfaces, Junit tests and GUIs. We prefer java because it’s the common language between us in group  

### 3.7.2 IDE (NetBeans)
#### 3.7.2.1 Description:
NetBeans is a software development platform. The NetBeans Platform allows applications to be developed from a set of modular software components called modules. 
#### 3.7.2.2 Uses:
We used NetBeans to write and compile java files in project. We discussed about the IDE which we will be used in project and we chose NetBeans between Eclipse, NetBeans because it's support JUnit test library which we used, it’s compatible with windows, Linux and it’s free to use you don’t need to purchase premium version, everyone in team had worked on it before.

### 3.7.3 Computer vision library (OpenCV)
#### 3.7.3.1 Description:
OpenCV (Open Source Computer Vision Library) is an open source computer vision and machine learning software library.
#### 3.7.3.2 Uses:
We used OpenCV library because it has many functions in image processing, it's work in real time and we generate some functions like save, load and flip 

### 3.7.4 GUI library (Swing)
#### 3.7.4.1 Description:
Swing is a set of program component s for Java programmers that provide the ability to create graphical user interface (GUI) components.
#### 3.7.4.2 Uses:
We used Swing library to generate three GUI windows (command, image and terminal). We used combo boxes to select which windows want to open and buttons to close, split horizontally, split vertically windows.we use it because it's support multi split panes 

### 3.7.5 Source code documentation (Javadoc)
#### 3.7.5.1 Description:
Javadoc is a documentation generator for the Java language. it comes with NetBeans to generate HTML and CSS website for the written code.
#### 3.7.5.2 Uses:
We used in Javadoc write the description of class in ‘/** */’ and its contain many tags like ‘@author’ to write the author of the created class, ‘@return’ to write the description what’s the function returns, ‘@version’ to write the version of class and ‘@since’ to write the date of the class created .

### 3.7.6 Version control system (Git)
#### 3.7.6.1 Description:
**Git** is source code management system for software development.
**GitLab** is an online Git repository manager. It is a great way to control over your projects. It allows editions in code, make issues to be solved.
#### 3.7.6.2 Uses:
We used GitLab to manage to whole project and keep tracking the editing in project and assign tasks for any members in team. We chose GitLab between GitHub, bit bucket because it’s free, private, members in project more than five people, we don’t need to make premium account to get addition features like in GitHub for example we must pay for have private project and in bit bucket the total number of must be five or less that five. 


### 3.7.7 Automated testing (JUnit)
#### 3.7.7.1 Description:
JUnit is a unit testing framework for the Java programming language. It checks all functions in class.
#### 3.7.7.2 Uses:
We used Junit to test all commands we created by checking if the image we have is matching the function output.

### 3.7.8 Build system (ant)
#### 3.7.8.1 Description:
Ant is a Java-based build tool created as part of the Apache open-source project. Ant scripts have a structure and are written in XML.
#### 3.7.8.2 Uses:
we use it to create jar file for classes we created

### 3.7.9 Tool for drawing diagrams (Easyuml)
#### 3.7.9.1 Description:
EasyUML is a UML diagram tool for NetBeans that provides features to easily create and work with UML diagrams.
#### 3.7.9.2 Uses:
we used EasyUML to draw class diagram, it's free and it comes with netbeans we don't need to look another tool.

### 3.7.10 Markdown
#### 3.7.10.1 Description:
Markdown is a lightweight markup language with plain text formatting syntax designed so that it can be converted to HTML and many other formats using a tool by the same name.
#### 3.7.10.2 Uses:
it helps us to generate our documentation and we use it to write report and readme.

## 3.8 Programming HOWTOs
### 3.8.1 How to create a new command

1- Click on `File` then choose `new file..` or press **(Ctrl + N)**.

2- Choose `Java` from categories and select file type `Java Class` then press `Next`.

3- Write class name on field `Class Name:` and choose `engine.commands` from `Package:` then press `Next`.

4- In line `public class NewClass` write after your **NewClass** `extends AbstractCommand`.

5- new window will opened **(asking for import engine.AbstractCommand)** press `ok` for add or press `cancel` then add it manually `import engine.AbstractCommand;`.

6- After import click on yellow bulb and choose `implement all abstract method` or **(Alt + Enter)** then choose `implement all abstract method` four methods will be created.

7-In Class `Engine.java` register the new command that you created in method `private Engine()` by writing `registerCommand(new NewClass());`.

### 3.8.2 How to create a new UI dockable

1- Click on `File` then choose `new file..` or press **(Ctrl + N)**.

2- Choose `Java` from categories and select file type `Java Class` then press `Next`.

3- Write class name on field `Class Name:` and choose `ui.dockables` from `Package:` then press `Next`.

4- In line `public class NewClass` write after your **NewClass** `extends JPanel implements Dockable`.

5- new window will opened **(asking for import ui.api.Dockable & import javax.swing.JPanel)** press `ok` or press `cancel` then add it manually `import javax.swing.JPanel;` & `import ui.api.Dockable;`.

6- After import click on yellow bulb and choose `implement all abstract method` or **(Alt + Enter)** then choose `implement all abstract method` two methods will be created.

7- In Class `GenericWindowContainer.java` register the new UI dockable that you created in `static` by writing `registerDockableClass(NewClass.class);`.

8- click on yellow bulb and choose `Add import for ui.dockables.NewClass` or **(Alt + Enter)** then choose `Add import for ui.dockables.NewClass` two methods will be created.


### 3.8.3 How to create a new test file

1- Click on `File` then choose `new file..` or press **(Ctrl + N)**.

2- Choose `Unit Tests` from categories and select file type `JUnit Test` then press `Next`.

3- Write class name on field `Class Name:` and choose `engine.commands` from `Package:` and  then press `Next`.
## 3.9 Testing
### 3.9.1 Test case ID: 1 
#### 3.9.1.1 Purpose: 
Using OpenCV functions through the terminal UI.
#### 3.9.1.2 System requirements covered: 
User can use all OpenCV functionalities by using the terminal.
#### 3.9.1.3 Preconditions: 
Terminal should be opened by the using the drop down menu containing all the UIs
#### 3.9.1.4 Inputs: 
Command name and command arguments
#### 3.9.1.5 post conditions: 
Command gets executed and a result is achieved
#### 3.9.1.6 user actions(steps):
1-  Write command name and arguments in a proper syntax.
2-  Press the enter key on the keyboard.
#### 3.9.1.7 Expected result:
1- The successfully executed command should appear in the terminal, 
2- In case of images, Image/modified image should be added to the dropdown menu of the images in image UI panel, 
3- In case of text, the text should appear in the terminal.
#### 3.9.1.8 Actual result:
1- The successfully executed command appeared in the terminal, 
2- In case of images, Image/modified images were added to the dropdown menu of the images in image UI panel, 
3- In case of text, the text appeared in the terminal.
#### 3.9.1.9 pass/fail/untested: 
Passed.
### 3.9.2 Test case ID: 2 
#### 3.9.2.1 Purpose:
Using OpenCV functions through the Command UI.
#### 3.9.2.2 System requirements covered: 
1- User can use all OpenCV functionalities by using the command interface, 
2- User can enter the arguments of the chosen command in the specified controls, 
3- User can click the execute button to execute the chosen command.
#### 3.9.2.3 Preconditions: 
Command UI should be opened by the using the drop down menu containing all the UIs
#### 3.9.2.4 Inputs:
Command name and command arguments
#### 3.9.2.5 post conditions:
Command gets executed and a result is achieved
#### 3.9.2.6 user actions(steps):
1-  User opens the Command UI panel.
2-  Choose a command from the dropdown menu.
3-  Write/choose the arguments for that command.
4-  Click on the execute button.
#### 3.9.2.7 Expected result:
Image/modified image should be added to the dropdown menu of the images in image UI panel.
#### 3.9.2.8 Actual result:
Image/modified images were added to the dropdown menu of the images in image UI panel. 
#### 3.9.2.9 pass/fail/untested:
Passed
### 3.9.3 Test case ID: 3 
#### 3.9.3.1 Purpose:
Having more than one panel aligning horizontally at the same time.
#### 3.9.3.2 System requirements covered:
User can split any panel horizontally.
#### 3.9.3.3 Preconditions:
User must click on the ‘%’ button.
#### 3.9.3.4 Inputs:
Mouse click on the ‘%’ button.
#### 3.9.3.5 post conditions:
One more panel appears horizontally. 
#### 3.9.3.6 user actions(steps):
1-  User click on the ‘%’ button in the desired panel that the user wants to split horizontally.
#### 3.9.3.7 Expected result:
The panel should be spitted in to two panels of the same type aligned horizontally
#### 3.9.3.7 Actual result:
The panel is spitted in to two panels of the same type aligned horizontally
#### 3.9.3.8 pass/fail/untested:
Passed.
### 3.9.4 Test case ID: 4 
#### 3.9.4.1 Purpose:
Having more than one panel aligning vertically at the same time.
#### 3.9.4.2 System requirements covered:
User can split any panel vertically
#### 3.9.4.3 Preconditions:
User must click on the ‘÷’ button.
#### 3.9.4.4 Inputs:
Mouse click on the ‘÷’ button
#### 3.9.4.5 post conditions:
One more panel appears vertically.
#### 3.9.4.6 user actions(steps):
1-  User click on the ‘÷’ button in the desired panel that the user wants to split vertically
#### 3.9.4.7 Expected result:
The panel should be spitted in to two panels of the same type aligned vertically.
#### 3.9.4.8 Actual result:
The panel is spitted in to two panels of the same type aligned vertically.
#### 3.9.4.9 pass/fail/untested:
Passed.
### 3.9.5 Test case ID: 5 
#### 3.9.5.1 Purpose:
Close any opened panel that the user doesn’t need anymore.
#### 3.9.5.2 System requirements covered:
User can close any panel.
#### 3.9.5.3 Preconditions:
User must click on the ‘x’ button.
#### 3.9.5.4 Inputs:
Mouse click on the ‘x’ button
#### 3.9.5.5 post conditions: 
Total number of opened panels decrease by one.
#### 3.9.5.6 user actions(steps): 
1-  User click on the ‘x’ button in the desired panel that the user wants to close.
#### 3.9.5.7 Expected result:
The panel should be closed.
#### 3.9.5.8 Actual result:
The panel is closed.
#### 3.9.5.9 pass/fail/untested:
Passed.
### 3.9.6 Test case ID: 6 
#### 3.9.6.1 Purpose:
Give the user some flexibility on how to use the application.
#### 3.9.6.2 System requirements covered:
User can choose which UI panel to open in each screen from the four UIs (About, Image, Command, and Terminal).
#### 3.9.6.3 Preconditions:
User should understand the functionalities of every UI panel.
#### 3.9.6.4 Inputs:
UI panel name.
#### 3.9.6.5 post conditions:
Chosen UI panel opens.
#### 3.9.6.6 user actions(steps):
1-  User clicks on the dropdown menu containing the UI panel names.
2-  User chooses what UI panel to open.
#### 3.9.6.7 Expected result:
The chosen UI panel should open in the panel that contains the dropdown menu that the user clicked on.
#### 3.9.6.8 Actual result:
The chosen UI panel is opened in the panel that contains the dropdown menu that the user clicked on.
#### 3.9.6.9 pass/fail/untested:
passed.
### 3.9.7 Test case ID: 7 
#### 3.9.7.1 Purpose:
Give the user flexibility to choose a certain command. 
#### 3.9.7.2 System requirements covered:
In the Command UI panel, user can choose a command.
#### 3.9.7.3 Preconditions:
User should understand the functionalities of every command.
#### 3.9.7.4 Inputs:
Command name.
#### 3.9.7.5 post conditions:
A set of UI components appear to the user for the chosen command.
#### 3.9.7.6 user actions(steps):
1 – Choose the command UI panel from the dropdown menu containing UI panel names.
2 – Choose a command name from the dropdown menu containing the command names.
#### 3.9.7.7 Expected result:
Some UI components of the chosen command arguments should appear for the user with an ‘execute button’
#### 3.9.7.8 Actual result:
Some UI components of the chosen command arguments appear for the user with an ‘execute button’
#### 3.9.7.9 pass/fail/untested:
passed.
### 3.9.8 Test case ID: 8 
#### 3.9.8.1 Purpose:
Give the user a way to see all the images he/she is working on.
#### 3.9.8.2 System requirements covered:
In Image UI panel, user can see all images stored in memory from a dropdown menu.
#### 3.9.8.3 Preconditions:
User should have at least loaded an image, user should open the Image UI panel.
#### 3.9.8.4 Inputs:
Image name
#### 3.9.8.5 post conditions:
Image names appears in the dropdown menu.
#### 3.9.8.6 user actions(steps):
1-  User loads an image.
2-  User opens the Image UI panel.
3-  User open the dropdown menu.
#### 3.9.8.7 Expected result:
All the image names in memory should appear in the dropdown menu. 
#### 3.9.8.8 Actual result:
All the image names in memory appear in the dropdown menu.
#### 3.9.8.9 pass/fail/untested:
passed
### 3.9.9 Test case ID: 9 
#### 3.9.9.1 Purpose:
Display images used by user to see the effect of other functions on.
#### 3.9.9.2 System requirements covered:
User can choose an image to display from the dropdown menu.
#### 3.9.9.3 Preconditions:
Some images should be stored in memory.
#### 3.9.9.4 Inputs:
Image names.
#### 3.9.9.5 post conditions:
Image loaded for the user
#### 3.9.9.6 user actions(steps):
1-  User puts some images in memory by using some functions.
2-  User opens the Image UI.
3-  User selects an image name from the dropdown menu.
#### 3.9.9.7 Expected result:
Selected image should be displayed in the panel
#### 3.9.9.8 Actual result:
Selected image is displayed in the panel
#### 3.9.9.9 pass/fail/untested:
passed.
### 3.9.10 Test case ID: 10 
#### 3.9.10.1 Purpose:
Give the user a way to check any modifications done on the image without the need to close and reopen the image.
#### 3.9.10.2 System requirements covered:
User can refresh/reload the displayed image.
#### 3.9.10.3 Preconditions:
An image is already displayed.
#### 3.9.10.4 Inputs:
Image name.
#### 3.9.10.5 post conditions:
New version of the same image displayed.
#### 3.9.10.6 user actions(steps):
1-  Choose an image to be displayed.
2-  Do some modification on the image.
3-  Click on the refresh button
#### 3.9.10.7 Expected result:
Displayed image should be refreshed and a newer version of the same image should be displayed.
#### 3.9.10.8 Actual result:
Displayed image gets refreshed and a newer version of the same image gets displayed.
#### 3.9.10.9 pass/fail/untested:
passed
### 3.9.11 Test case ID: 11 
#### 3.9.11.1 Purpose:
let the user know about the application.
#### 3.9.11.2 System requirements covered:
In About UI, user can see a description of the application.
#### 3.9.11.3 Preconditions:
user opens the About UI panel.
#### 3.9.11.4 Inputs:
UI panel name.
#### 3.9.11.5 post conditions:
About UI panel containing information about the application
#### 3.9.11.6 user actions(steps):
1-  User chooses the About UI panel from the dropdown menu.
#### 3.9.11.7 Expected result:
About UI panel should be opened.
#### 3.9.11.8 Actual result:
About UI panel should is opened.
#### 3.9.11.9 pass/fail/untested:
passed.
### 3.9.12 Test case ID: 12 
#### 3.9.12.1 Purpose:
let the user know the position of a certain pixel.
#### 3.9.12.2 System requirements covered:
user can check on the pixel position (x,y).
#### 3.9.12.3 Preconditions:
an image is already displayed to the user.
#### 3.9.12.4 Inputs:
mouse click on the pixel.
#### 3.9.12.5 post conditions:
the pixel's values of X and Y are displayed to the user.
#### 3.9.12.6 user actions(steps):
1-  User puts an image in the memory
2-  User opens the Image UI panel.
3-  User display an image.
4-  User clicks on any place in the image.
#### 3.9.12.7 Expected result:
X and Y values for the position that the user clicked on should be displayed.
#### 3.9.12.8 Actual result:
X and Y values for the position that the user clicked was displayed.
#### 3.9.12.9 pass/fail/untested:
passed.
### 3.9.13 Test case ID: 13 
#### 3.9.13.1 Purpose:
Lets the user re-size an opened imaged dynamically through the Image UI.
#### 3.9.13.2 System requirements covered:
User can resize any opened image
#### 3.9.13.3 Preconditions:
An image is already displayed to the user, At least 2 panels are opened and one of them is an Image UI panel.
#### 3.9.13.4 Inputs:
click and drag any border of any opened panel.
#### 3.9.13.5 post conditions:
Displayed image gets re-sized dynamically.
#### 3.9.13.6 user actions(steps):
1-  User puts an image in the memory
2-  User opens the Image UI panel.
3-  User display an image.
4-  User opens another panel or more of any type.
5-  User clicks and drags any border of any opened panel.
#### 3.9.13.7 Expected result:
The displayed image/s get re-sized while the user is dragging the border.
#### 3.9.13.8 Actual result:
The displayed image/s got re-sized while the user was dragging the border.
#### 3.9.13.9 pass/fail/untested:
passed.
### 3.9.14 Test case ID: 14 
#### 3.9.14.1 Purpose:
Makes the user able to search for a specific command in the command UI.
#### 3.9.14.2 System requirements covered:
User can search for a specific command in the command UI.
#### 3.9.14.3 Preconditions:
The command UI should be opened.
#### 3.9.14.4 Inputs:
Sequence of characters that forms a command name
#### 3.9.14.5 post conditions:
The user selects the command that he searched for.
#### 3.9.14.6 user actions(steps):
1-  User Opens the Command UI.
2-  User writes the command name in the combo box containing all the command names.
3-  User selects the command that is the result of the searching process.
#### 3.9.14.7 Expected result:
The gui components for the command that was searched for gets displayed in the command UI. 
#### 3.9.14.8 Actual result:
The gui components for the command that was searched for got displayed in the command UI.
#### 3.9.14.9 pass/fail/untested:
passed.

# 3.10 Faced Problems
There are two types of problems.
## 3.10.1 Technical Problems.
### 3.10.1.1 Load OpenCV library on different operating systems.
### 3.10.1.2 Different versions of OpenCV and merging codes generated by each one of them. 
Using "Git" technology.
### 3.10.1.3 Determining which service provider to use for "Git" technology
Choosing between (GitHub,Bitbucket,...etc.)

## 3.10.2 Non-Technical Problems
### 3.10.2.1 Not able to use some service providers For money and members number issues
Not being able to use GitHub private project because it require payment
Not being able to use Bitbucket because max number for members is 5 while actual number of the project team is 6 including team supervisor




# 4. Current semester changes
## 4.1 Scope
## 4.2 Architectural changes

The current architecture that we used to develop the prototype has shown
considerable success in making the prototype easy to use and maintain. However,
several refinements are needed in order to achieve a modular design. The intention
was to let our product reflect as much as possible of the internals of the
wrapped library (OpenCV in our demo), and at the same time sustain ease of use for
the average user.

### 4.2.1 Plugin support
Not only should users be able to use the available functions in the library, but
also to extend them by adding their own ones. Here is the workflow of the procedure

1. Creates a new class in a separate project containing the functions he wishes
to add.
2. Compiles the source into a `.class` file.
3. Optionally, pack the `.class` file into a `.jar` file.
4. Register the class in the engine by typing `load_class /path/to/class`.

Applying the above steps should be enough for the user to register his new classes.
The user should not care about the interpreter or the UI, since it is the role
of the engine to notify the other wrappers.


## 5. Roles
### 5.1 Abdelrahman Mohsen
- Report: Javadoc, UML Diagrams and Design Patterns
- Prototype engine: CmdImgLoad.java, CmdThreshold.java, CmdAddWeighted.java, CmdBilateralFilter.java, CmdBoxFilter.java, CmdCornerHarris.java, CmdDilation.java, CmdDistanceTransform.java, CmdFilter2D.java,CmdErosion.java, CmdScharr.java CmdImgLoadTest.java
- Prototype GUI: Command.java , ArgumentEditors.java, CmdIdEditors.java, ImgIdEditors.java, StringEditor.java, SystemPathEditor.java, IntEditor.java, SmallIntEditor.java, FloatEditor.java.  

### 5.2 Al Mohannad Haroon
- Report: new technologies, faced problems and Methodologies 
- Prototype engine: CmdImgFree.java, CmdImgFlip.java, CmdImgFreeTest.java, CmdBitwiseAnd.java, CmdBitwiseNot.java, CmdBitwiseOr.java, CmdBitwiseXor.java,
  CmdDivide2.java, CmdDivide3.java, CmdImgFlip.java, CmdImgFree.java, CmdImgNormalize.java, CmdImgReduce.java, CmdImgRepeat.java, CmdMaximum.java, CmdMinimum.java, CmdMultiply2.java, CmdRaisesToPower.java
- GUI: execute button

### 5.3 Amr Ayman
- Report: Functional requirments, TestCases.
- Engine: interpreter.java, CmdEqualize.java, CmdImgCapture.java, CmdAbsdiff.java, CmdColor.java, CmdDrawCircle.java, CmdGaussianBlur.java, CmdDrawHoughCircles.java , CmdHistogram.java, CmdHoughCircles.java, CmdaccumulateSquare.java, Cmdresize.java, Cmdaccumulate.java, CmdImgCapture.java, CmdLaplacian.java, CmdMan.java, CmdMorph.java, CmdmatchTemplate.java, CmdPyr.java, CmdcopyMakeBorder.java, CmdRandShuffle.java. 
- GUI: Terminal GUI.

### 5.4 Amr Gamal
- Report: Technology used, Roles and How to
- Prototype engine: CmdImgSave.java, CmdImgEdge.java, CmdCanny.java, CmdBlur.java, CmdFaceDetection.java, CmdImgSaveTest.java, CmdAdd.java, CmdInRange.java, CmdMedianBlur.java, CmdPutText.java, CmdTranspose.java, CmdMultiply,CmdDivide, CmdImgFlipTest.java and  CmdImgEdgeTest.java
- GUI: Image.java.

### 5.5 Mohammad Helmy
- Report: abstract, architectural changes, user manual, compilation and installation manual
- Engine: Engine.java, AbstractCommand.java, ICommand.Java, Parameter.java, Argument.java, CmdSubtract.java, support plugins
- GUI: Implemented a new docking framework, supports splitting, resizing and closing windows. Better alignment of editors in command panel, auto-resize images in Image panel, show currently selected path in path editor, better packing of navigation components
- Continuos integration script