By working on this project, it is expected that you will learn, if you don't
already know, the following implementation skills.

1. Programming using Java
2. Building Java projects using NetBeans or Ant
3. Image processing using OpenCV
4. Building dynamic GUI applications using Swing and AWT.
5. Source code documentation using Doxygen
6. Version control using Git
7. Issue/bug tracking on gitlab.com
8. Automated testing using JUnit
9. Logging using Log4j
10. Internationalization (nice to have)
